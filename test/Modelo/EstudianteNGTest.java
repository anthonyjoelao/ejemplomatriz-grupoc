/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Util.ExceptionUFPS;
import static org.testng.Assert.*;
import org.testng.annotations.Test;

/**
 *
 * @author madar
 */
public class EstudianteNGTest {
    
    private Estudiante estd_Prueba, estd_Prueba2;
    
    public EstudianteNGTest() {
    }

    public void escenario() throws ExceptionUFPS
    {
        this.estd_Prueba=new Estudiante(115,"madarme","3.5,4,5");
        //vector_quices={3.5,4,5}--> Ideal
    }
    
    
    public void escenario2() throws ExceptionUFPS
    {
        this.estd_Prueba2=new Estudiante(116,"njadarme","5,5,2,1");
        //vector_quices={5,5,2,1}--> Ideal
    }
    
    
    @Test
    public void testGetCodigo() throws ExceptionUFPS {
        System.out.println("getCodigo");
        //1. Invocar el escenario
        this.escenario();
        //2. Colocar el valor esperado:
        long expResult = 115;
        long result = this.estd_Prueba.getCodigo();
        assertEquals(result, expResult);
        //fail("The test case is a prototype.");
    }

    @Test
    public void testGetQuices() throws ExceptionUFPS
    {
    System.out.println("getQuices");
        //1. Invocar el escenario
        this.escenario();
        //2. colocar el valor esperado:
        //vector_quices={3.5,4,5}--> Ideal
        float vector_quices_ideal[]={3.5F,4F,5F};
        float vector_de_prueba[]=this.estd_Prueba.getQuices();
        //Comparar los resultados:
        // si son iguales vector_quices_ideal con vector_prueba --> :)
        //No puedo hacer esto:
        // if (vector_quices_ideal == vector_de_prueba)
        //  estaría comparando si la dirbase del 1 vector es igual a la del 2° vector siendo un error, por que deseo comparar sus contenidos
        boolean resultado=this.sonIgualesVectores(vector_de_prueba, vector_quices_ideal);
        assertEquals(resultado, true);
    }
   
    @Test
    public void testGetQuices2() throws ExceptionUFPS
    {
    System.out.println("getQuices2");
        //1. Invocar el escenario
        this.escenario2();
        //2. colocar el valor esperado:
        //vector_quices={5,5,2,1}--> Ideal
        float vector_quices_ideal[]={5,5,2,1};
        float vector_de_prueba[]=this.estd_Prueba2.getQuices();
        //Comparar los resultados:
        // si son iguales vector_quices_ideal con vector_prueba --> :)
        //No puedo hacer esto:
        // if (vector_quices_ideal == vector_de_prueba)
        //  estaría comparando si la dirbase del 1 vector es igual a la del 2° vector siendo un error, por que deseo comparar sus contenidos
        boolean resultado=this.sonIgualesVectores(vector_de_prueba, vector_quices_ideal);
        assertEquals(resultado, true);
    }
   
    
    
    
    private boolean sonIgualesVectores(float v1[], float v2[])
    {
    if(v1.length!=v2.length)
        return false;
    for(int i=0;i<v1.length;i++)
        {
            if(v1[i]!=v2[i])
                return false;
        }
    return true;
    }
    
  @Test
    public void testGetPromedio() throws ExceptionUFPS {
    System.out.println("getPromedio");  
    //1. Escenario
    this.escenario();
    //2. Ideal
    float prom_esperado=((3.5F+4F+5F)/3);
    //3. Resultado:
    float resultado=this.estd_Prueba.getPromedio();
    //4. Ejecutar la prueba:
    assertEquals(resultado, prom_esperado);
    }
      
    
}
