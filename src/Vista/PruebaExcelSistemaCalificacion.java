/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Negocio.SistemaCalificaciones;
import java.io.IOException;

/**
 *
 * @author madar
 */
public class PruebaExcelSistemaCalificacion {
    
    public static void main(String[] args) throws IOException, Exception {
        SistemaCalificaciones sistema=new SistemaCalificaciones("src/Datos/estudiantes.xls");
        //System.out.println(sistema.toString());
        //Como consulta revisar la clase DecimalFormat
        String promedios[]=sistema.getPromedio();
        for(String datoPromedio:promedios)
                System.out.println(datoPromedio);
        
        System.out.println("Generando pdf:");
        sistema.crearPDF_Promedios();
        System.out.println("PDF creado...");
    }
            
    
}
